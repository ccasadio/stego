# README #

Steganography is the art or practice of concealing a file, message, image, or video within another file, message, image, or video. This program implements a form of least significant bit modification on each pixel to encode files into images and decode files from images. 

## Usage ##
input  | output
------------- | -------------
file  | image version of the file
file and image  | same image modified to contain file data
image  | file contained in image

## Notes ##
* File compression and decompression has yet to be implemented but is planned and has a spot in the code.
* Data encryption and decryption has yet to be implemented but is planned and has a spot in the code.
* The amount of distortion to the image is based on the size of the image relative to the file.
* No checking for non-modified images currently implemented.

### Who do I talk to? ###

* casadiochristian@gmail.com
* cmicklis@stetson.edu