package fileStream;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.Header;
import main.Model;
import main.Transformations;

public class SampleStreamUsage 
{
	public static void main(String[] args)
	{
		String imgLoc = "C:/Users/christian/Desktop/1.jpeg";
		String fileLoc = "C:/Users/christian/Desktop/1.txt";
		String fileSaveLoc = "C:/Users/christian/Desktop/";
		String password = null;
		
		try {
			encodeImage(fileLoc, imgLoc);
		} catch (IOException e) {e.printStackTrace();}
		
		try {
			Model.pullDataFromImage(imgLoc, fileSaveLoc, password);
		} catch (FileNotFoundException e) {e.printStackTrace();}
	}
	
	public static void encodeImage(String fileName, String imageName) throws IOException
	{
		File file = new File(fileName), imageLoc = new File(imageName);
		BufferedImage image = ImageIO.read(imageLoc);
		int imageLength = image.getHeight()*image.getWidth();
		int base = Transformations.calculateMinimumBase((int)file.length(), imageLength*3);
		SteganographyBufferedInputStream in = new SteganographyBufferedInputStream(fileName, base);
		Header header = new Header();
		header.encodeBase.setEncodeBase(base);
		header.encodeEncryption.setEncodeEncryption(false);
		header.encodeSize.setEncodeSize((int)file.length()*Transformations.baseNByteLength(base));
		header.encodeType.setEncodeType(1);
		String[] manipulatedFileName = fileName.split("\\.");
		header.fileExtension.setFileExtension("bigbootyismyjam." + manipulatedFileName[manipulatedFileName.length - 1]);
		
		int[] headerData = header.getData();
		int[] pixel = null;
		int[] transformedPixel = new int[3];
		
		int iter, p = 0;
		for(iter = 0; iter < headerData.length; iter++)
		{
			if(iter%3 == 0)
			{
				pixel = pixelRGB(image.getRGB((iter/3)%image.getWidth(), (iter/3)/image.getWidth()));
			}
			
			transformedPixel[iter%3] = Transformations.encodeDataIntoValue(pixel[iter%3], headerData[iter], header.getHeaderBase());
			System.out.println(pixel[iter%3] + " " + transformedPixel[iter%3] + " " + (pixel[iter%3] - pixel[iter%3]%header.getHeaderBase() + headerData[iter]));
			
			if(iter%3 == 2)
			{
				image.setRGB((iter/3)%image.getWidth(), (iter/3)/image.getWidth(), pixelRGB(transformedPixel));
			}
		}
		
		System.out.println(iter);
		
		if(iter%3 == 1)
		{
			pixel[1] = Transformations.encodeDataIntoValue(pixel[1], in.read(), base);
			pixel[2] = Transformations.encodeDataIntoValue(pixel[2], in.read(), base);
			image.setRGB((iter/3)%image.getWidth(), (iter/3)/image.getWidth(), pixelRGB(pixel));
			p = 1;
		}
		else if(iter%3 == 2)
		{
			pixel[2] = Transformations.encodeDataIntoValue(pixel[2], in.read(), base);
			image.setRGB((iter/3)%image.getWidth(), (iter/3)/image.getWidth(), pixelRGB(pixel));
			p = 1;
		}
		
		
		for(int i = (headerData.length + p)/3; i < imageLength; i++)
		{
			pixel = pixelRGB(image.getRGB(i%image.getWidth(), i/image.getWidth()));
			
			pixel[0] = Transformations.encodeDataIntoValue(pixel[0], in.read(), base);
			pixel[1] = Transformations.encodeDataIntoValue(pixel[1], in.read(), base);
			pixel[2] = Transformations.encodeDataIntoValue(pixel[2], in.read(), base);
			
			image.setRGB(i%image.getWidth(), i/image.getWidth(), pixelRGB(pixel));
		}
		
		ImageIO.write(image, "png", imageLoc);
	}
	
	public static void decodeImage(String fileSaveLocation, String imageLocation) throws FileNotFoundException
	{
		int base = 0;
		SteganographyBufferedOutputStream out = new SteganographyBufferedOutputStream(fileSaveLocation, base);
	}
	
	//helper methods for working with BufferedImages
	private static int[] pixelRGB(int pixel) 
	{
//		int alpha = (pixel >> 24) & 0xff;
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		int[] id = {red, green, blue};
		return id;
	}
	
	private static int pixelRGB(int[] pixel) 
	{
		return ((pixel[0]&0x0ff)<<16)|((pixel[1]&0x0ff)<<8)|(pixel[2]&0x0ff);
	}
}
