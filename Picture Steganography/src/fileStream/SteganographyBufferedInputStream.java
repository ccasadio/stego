package fileStream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import main.Transformations;


public class SteganographyBufferedInputStream extends SteganographyStream
{
	private InputStream myInStream;
	
	
	public SteganographyBufferedInputStream(String fileName, int base) throws FileNotFoundException
	{
		super(base);
		myInStream = new BufferedInputStream(new FileInputStream(fileName));
	}
	
	public SteganographyBufferedInputStream(String fileName, int base, String password) throws FileNotFoundException
	{
		this(fileName, base);
		
		try
		{
            password = super.modifyPassword(password);
            SecretKey aesKey = new SecretKeySpec(password.getBytes(), "AES");
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);
	        
	        myInStream = new CipherInputStream(myInStream, aesCipher);
		}
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchPaddingException e) 
		{
			e.printStackTrace();
		}
		catch (InvalidKeyException e) 
		{
			e.printStackTrace();
		}
	}
	
	public int read() throws IOException
	{
		if(myCounter == BYTELENGTH - 1)
		{
			myData = Transformations.base10ByteToBaseN(BASE, myInStream.read());
			
			return myData[myCounter = 0];
		}

		return myData[++myCounter];
	}
	
//	public int read() throws IOException
//	{
//		if(myCounter == BYTELENGTH - 1)
//		{
//			myData[0] = myInStream.read();
//			for(int i = BYTELENGTH - 1; i >= 1 ; i--)
//			{
//				myData[i] = myData[0]%BASE;
//				myData[0] = (int) Math.floor(myData[0]/BASE);
//			}
//			
//			return myData[myCounter = 0];
//		}
//
//		return myData[++myCounter];
//	}
}
