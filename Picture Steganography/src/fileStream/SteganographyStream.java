package fileStream;

import main.Transformations;

public abstract class SteganographyStream 
{
	private static final String PADDING = "bfhdjsabfjhkdsabhjkfbdasjhfhvgjhcvhgjvghvgjhvghjvghjvjghbhdajskbfhdask"; //needs improvement
	private static final int MAX_AES_KEY_LENGTH = 16;
	protected final int BASE, BYTELENGTH;
	protected int myCounter;
	protected int[] myData;
	
	public SteganographyStream(int base)
	{
		BASE = base;
		BYTELENGTH = Transformations.baseNByteLength(base); 
		myData = new int[BYTELENGTH];
		myCounter = BYTELENGTH - 1;
	}
	
	protected String modifyPassword(String password) //needs improvement
	{
		if (password.length() < MAX_AES_KEY_LENGTH)
        {
        	password += PADDING.substring(0, MAX_AES_KEY_LENGTH - password.length());
        }
        else
        {
        	password = password.substring(0, MAX_AES_KEY_LENGTH);
        }
		return password;
	}
}
