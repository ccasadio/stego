package fileStream;

import main.Transformations;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class SteganographyBufferedOutputStream extends SteganographyStream
{
	private OutputStream myOutStream;
	
	public SteganographyBufferedOutputStream(String fileSaveLocation, int base) throws FileNotFoundException
	{
		super(base);
		myOutStream = new BufferedOutputStream(new FileOutputStream(fileSaveLocation));
	}
	
	public SteganographyBufferedOutputStream(File file, int base) throws FileNotFoundException
	{
		super(base);
		myOutStream = new BufferedOutputStream(new FileOutputStream(file));
	}
	
	public SteganographyBufferedOutputStream(String fileSaveLocation, int base, String password) throws FileNotFoundException
	{
		this(fileSaveLocation, base);
		try
		{
            password = super.modifyPassword(password);
            SecretKey aesKey = new SecretKeySpec(password.getBytes(), "AES");
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);
            myOutStream = new CipherOutputStream(myOutStream, aesCipher);
		}
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchPaddingException e) 
		{
			e.printStackTrace();
		}
		catch (InvalidKeyException e) 
		{
			e.printStackTrace();
		}
	}
	
	public SteganographyBufferedOutputStream(File file, int base, String password) throws FileNotFoundException
	{
		this(file, base);
		try
		{
            password = super.modifyPassword(password);
            SecretKey aesKey = new SecretKeySpec(password.getBytes(), "AES");
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);
            myOutStream = new CipherOutputStream(myOutStream, aesCipher);
		}
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchPaddingException e) 
		{
			e.printStackTrace();
		}
		catch (InvalidKeyException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void write(int b) throws IOException
	{
		myData[myCounter++] = b;
		if(myCounter == BYTELENGTH - 1)
		{
			myOutStream.write(Transformations.baseNByteToBase10(BASE, myData));
			myCounter = 0;
		}
	}
	
	public void flush() throws IOException
	{
		myOutStream.flush();
		if(myCounter != 0)
		{
			throw new IllegalStateException("Data length mismatch.");
		}
	}
	
	public void close() throws IOException
	{
		myOutStream.close();
	}
}
