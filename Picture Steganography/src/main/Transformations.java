package main;

/**
 * This class consists exclusively of static methods that do data transformations
 * related to steganography.
 * 
 * @author Christian Casadio
 *
 */
public class Transformations 
{
	/**
	 * Manipulates a value such that the value%base = data, with an upper limit constraint of valueMax. 
	 * The original value will not be preserved but the returned value will be in the interval [value - base, value + base) 
	 * unless the returned value is > valueMax in which case the interval is [value - 2*base, value).
	 * 
	 * <p>Example: value = 56, data = 3, base = 4, valueMax = 255 would return 55.
	 * 
	 * @param value int to be modified to contain data.
	 * @param data int to be encoded into value.
	 * @param base Base of number system that data is in.
	 * @param valueMax Max value that can be returned.
	 * @return int that is close to value and is subject to the constraints return%base = data and return <= valueMax.
	 * @throws IllegalArgumentException is thrown if value > valueMax or data >= base.
	 */
	public static int encodeDataIntoValue(int value, int data, int base, int valueMax)
	{
		if(value > valueMax)
		{
			throw new IllegalArgumentException("value can't exceed value maximum.");
		}
		else if(data >= base)
		{
			throw new IllegalArgumentException("data can't exceed or equal base.");
		}
		
		value = value - value%base + data;
		
		if(value > valueMax)
		{
			return value - base;
		}
		
		return value;
	}
	
	/**
	 * Manipulates a value such that the value%base = data, with an upper limit constraint of 255. 
	 * The original value will not be preserved but the returned value will be in the interval [value - base, value + base) 
	 * unless the returned value could be > 255 in which case the interval is [value - 2*base, value).
	 * 
	 * <p>Example: value = 56, data = 3, base = 4 would return 55.
	 * 
	 * @param value int to be modified to contain data.
	 * @param data int to be encoded into value.
	 * @param base Base of number system that data is in.
	 * @return int that is close to value and is subject to the constraints return%base = data and return <= 255.
	 * @throws IllegalArgumentException is thrown if value > 255 or data >= base.
	 */
	public static int encodeDataIntoValue(int value, int data, int base)
	{
		return encodeDataIntoValue(value, data, base, 255);
	}
	
	/**
	 * Converts an int [0, 255] from base 10 number system to the specified base number system, 
	 * where each digit is in a different array position. 100 in base 10 would be represented as 
	 * [1,0,0].
	 * @param base Base of the number system that the data is being transformed into.
	 * @param length Number of digits in the returned number.
	 * @param data Number to be transformed.
	 * @return int[] of digits.
	 * @throws IllegalArgumentException if base < 2.
	 * @throws IllegalStateException if length is too short to represent data in the given base.
	 */
	public static int[] base10ByteToBaseN(int base, int length, int data)
	{
		if(base < 2)
		{
			throw new IllegalArgumentException("Input base too small.");
		}
		
		int[] transformedData = new int[length];
		
		int numberToBeChanged = data;
		for(int i = length - 1; i >= 0 ; i--)
		{
			transformedData[i] = numberToBeChanged%base;
			numberToBeChanged = (int) Math.floor(numberToBeChanged/base);
		}
		
		if(transformedData[length - 1] >= base)
		{
			throw new IllegalStateException("Transformation failed: last byte digit is " + transformedData[length - 1] + " with a base of " + base + ".");
		}
		
		return transformedData;
	}
	
	/**
	 * Converts an int [0, 255] from base 10 number system to the specified base number system, 
	 * where each digit is in a different array position. 100 in base 10 would be represented as 
	 * [1,0,0].
	 * 
	 * @param base Base of the number system that the data is being transformed into.
	 * @param data Number to be transformed.
	 * @return int[] of digits.
	 * @throws IllegalArgumentException if base < 2 or data > 255.
	 */
	public static int[] base10ByteToBaseN(int base, int data)
	{
		return base10ByteToBaseN(base, baseNByteLength(base), data);
	}
	
	/**
	 * Converts an int [0, 255] from the specified base number system to base 10.
	 * 
	 * @param base Base of number system that the data is in.
	 * @param data A number in the specified base where each digit is stored in 
	 * a different position in the array.
	 * @return The base 10 representation of the input number.
	 * IllegalArgumentException if base < 2.
	 */
	public static int baseNByteToBase10(int base, int[] data)
	{
		if(base < 2)
		{
			throw new IllegalArgumentException("Input base too small.");
		}
		
		int temp = 0;
		
		for(int i = 0; i < data.length; i++)
		{
			temp = temp * base;
			temp = temp + data[i];
		}
		
		return temp;
	}
	
	/**
	 * Gives the number of digits required to represent a number [0, 255] in the specified base.
	 * 
	 * @param base Base of number system.
	 * @return Number of digits required to represent a number [0, 255] in the specified base.
	 * IllegalArgumentException if base < 2.
	 */
	public static int baseNByteLength(int base)
	{
		if(base < 2)
		{
			throw new IllegalArgumentException("base too small");
		}
	    else if(base == 2)
		{
			return 8;
		}
		else if(base == 3)
		{
			return 6;
		}
		else if(base <= 6)
		{
			return 4;
		}
		else if(base <= 15)
		{
			return 3;
		}
		else if(base <= 255)
		{
			return 2;
		}
		else if(base >= 256)
		{
			return 1;
		}
		
		return 0;
	}
	
	public static int calculateMinimumBase(int sizeData, int sizeStorage)
	{
		int base = 0;
		int b = 2;
		while(base == 0)
		{
			if(sizeData * Transformations.baseNByteLength(b) < sizeStorage)
			{
				base = b;
			}
			
			if(b > 16)
			{
				throw new IllegalArgumentException("File too large."); //needs a more specific exception
			}
					
			b++;
		}
		return base;
	}
}
