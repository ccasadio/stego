package main;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;


public class Model 
{
	private static Header myHeader = new Header(4, 1, 4, 1, 8, 12);
	private static String PADDING = "bfhdjsabfjhkdsabhjkfbdasjhfhvgjhcvhgjvghvgjhvghjvghjvjghbhdajskbfhdask";
	private static int MAX_AES_KEY_LENGTH = 16;
	
	//options for encode type
	private final static int DataMap = 0, BaseN = 1;
	
	
	public static void modifyImage(String imgLoc, String fileLoc, String password)
	{
		BufferedImage img = loadImage(imgLoc);
		ArrayList<Integer> data = loadFile(fileLoc, password);
		
		int base = encodeBase(data.size(), img.getHeight() * img.getWidth() * 3);
		data = transformDataToBaseN(data, base);
		
		setHeader(BaseN, base, false, fileLoc, data.size());
		
		//logic to modify each pixel such that val%base == convertedFileData
		Random rand = new Random();
		int[] header = myHeader.getData();
		int count = 0;
		for(int i = 0; i < img.getWidth(); i ++)
		{
			for(int j = 0; j < img.getHeight(); j++)
			{
				int[] pixel = pixelRGB(img.getRGB(i, j));
				for(int k = 0; k < 3; k++)
				{
					if(pixel[k] + base > 255)
					{
						pixel[k] = pixel[k] - base;
					}
					if(count < myHeader.length())
					{
						pixel[k] = pixel[k] - pixel[k]%myHeader.getHeaderBase() + header[count];
					}
					else if(count >= data.size() + myHeader.length())
					{
						pixel[k] = pixel[k] - pixel[k]%base + rand.nextInt(base);
					}
					else
					{
						pixel[k] = pixel[k] - pixel[k]%base + data.get(count - myHeader.length());
					}
					count++;
				}
				img.setRGB(i, j, pixelRGB(pixel));
			}
		}
		saveImage(img,imgLoc);
	}
	
	public static void createDataImage(String imgLoc, String fileLoc, String password)
	{
		BufferedImage img = null;
		ArrayList<Integer> data = loadFile(fileLoc, password);
		
		int base = 256;
		
		setHeader(DataMap, base, false, fileLoc, data.size());
		
		//logic to modify each pixel such that val%base == convertedFileData
		Random rand = new Random();
		int x = (int) Math.ceil(Math.sqrt((data.size() + myHeader.length())/3));
		img = new BufferedImage(x, x, BufferedImage.TYPE_INT_RGB);
		int[] header = myHeader.getData();
		int count = 0;
		for(int i = 0; i < x; i ++)
		{
			for(int j = 0; j < x; j++)
			{
				int[] pixel = new int[3];
				for(int k = 0; k < 3; k++)
				{
					if(count < myHeader.length())
					{
						pixel[k] = header[count];
					}
					else if(count >= data.size() + myHeader.length())
					{
						pixel[k] = rand.nextInt(256);
					}
					else
					{
						pixel[k] = data.get(count - myHeader.length());
					}
					count++;
				}
				img.setRGB(i, j, pixelRGB(pixel));
			}
		}
		saveImage(img,imgLoc);
	}
	
	public static void pullDataFromImage(String imgLoc, String fileSaveLoc, String password) throws FileNotFoundException
	{
		BufferedImage img = loadImage(imgLoc);
		ArrayList<Integer> data = new ArrayList<Integer>();
		
		boolean done = false;
		int headerBase = myHeader.getHeaderBase();
		int count = 0;
		for(int i = 0; i < img.getWidth() && !done; i ++)
		{
			for(int j = 0; j < img.getHeight() && !done; j++)
			{
				int[] pixel = pixelRGB(img.getRGB(i, j));
				for(int k = 0; k < 3 && !done; k++)
				{
					if(count < myHeader.length())
					{
						myHeader.setData(count, pixel[k] % headerBase);
					}
					else if(count <= myHeader.encodeSize.getEncodeSize() + myHeader.length())
					{
						data.add(pixel[k]);
					}
					else
					{
						done = true;
					}
					count++;
				}
			}
		}
		
		if(myHeader.encodeType.getEncodeType() == BaseN)
		{
			int base = myHeader.encodeBase.getEncodeBase();
			for(int i = 0; i < data.size(); i++)
			{
				data.set(i, data.get(i)%base);
			}
			data = transformDataFromBaseN(data, base);
		}
		saveFile(fileSaveLoc += myHeader.fileExtension.getFileExtension(), data, password);
	}
	
	
	//-------------------------------------------------------------------------------------------------------
	public static void main(String[] args)
	{
		String imgLoc = "C:/Users/christian/Desktop/3.png";
		String fileLoc = "C:/Users/christian/Desktop/1.pdf";
		String fileSaveLoc = "C:/Users/christian/Desktop/";
		String password = null;
		//modifyImage(imgLoc, fileLoc, password);
		
		try {
			pullDataFromImage(imgLoc, fileSaveLoc, password);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	
	@SuppressWarnings("resource")
	private static ArrayList<Integer> loadFile(String fileLocation, String password)
	{
		BufferedInputStream bis = null;
		try 
		{
			bis = new BufferedInputStream(new FileInputStream(fileLocation));
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		
		InputStream is = null;
		
		if(password != null)
		{
			try
			{
	            if (password.length() < MAX_AES_KEY_LENGTH)
	            {
	            	password += PADDING.substring(0, MAX_AES_KEY_LENGTH - password.length());
	            }
	            else
	            {
	            	password = password.substring(0, MAX_AES_KEY_LENGTH);
	            }
	            byte[] beep = password.getBytes();
	            SecretKey aesKey = new SecretKeySpec(beep, "AES");
	            Cipher aesCipher = Cipher.getInstance("AES");
	            aesCipher.init(Cipher.ENCRYPT_MODE, aesKey);
	            
	            is = new CipherInputStream(bis, aesCipher);
			}
			catch (NoSuchAlgorithmException e) 
			{
				e.printStackTrace();
			} 
			catch (NoSuchPaddingException e) 
			{
				e.printStackTrace();
			}
			catch (InvalidKeyException e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			is = bis;
		}
		
		ArrayList<Integer> al = new ArrayList<Integer>();
		try 
		{
			for(int i = is.read(); i != -1; i = is.read())
			{
				al.add(i);
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return al;
	}
	
	private static void saveFile(String fileSaveLocation, ArrayList<Integer> fileData, String password) throws FileNotFoundException
	{
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileSaveLocation));
		
		OutputStream os = null;
		
		if(password != null)
		{
			try
			{

	            if (password.length() < MAX_AES_KEY_LENGTH)
	            {
	            	password += PADDING.substring(0, MAX_AES_KEY_LENGTH - password.length());
	            }
	            else
	            {
	            	password = password.substring(0, MAX_AES_KEY_LENGTH);
	            }
	            byte[] pwBytes = password.getBytes();
	            SecretKey aesKey = new SecretKeySpec(pwBytes, "AES");
	            Cipher aesCipher = Cipher.getInstance("AES");
	            aesCipher.init(Cipher.DECRYPT_MODE, aesKey);

	            os = new CipherOutputStream(bos, aesCipher);
			}
			catch (NoSuchAlgorithmException e) 
			{
				e.printStackTrace();
			} 
			catch (NoSuchPaddingException e) 
			{
				e.printStackTrace();
			}
			catch (InvalidKeyException e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			os = bos;
		}
		
		for(int i = 0; i < fileData.size(); i++)
		{
			try 
			{
				os.write(fileData.get(i));
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		
		try 
		{
			os.flush();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		try 
		{
			os.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private static int encodeBase(int sizeData, int sizeStorage)
	{
		int base = 0;
		int b = 2;
		while(base == 0)
		{
			if(sizeData * Transformations.baseNByteLength(b) < sizeStorage)
			{
				base = b;
			}
			
			if(b > 16)
			{
				System.err.println("file too large");
			}
					
			b++;
		}
		return base;
	}
	
	private static ArrayList<Integer> transformDataToBaseN(ArrayList<Integer> data, int base)
	{
		ArrayList<Integer> convertedFileData = new ArrayList<Integer>();
		ArrayList<Integer> tempList = new ArrayList<Integer>();
		int byteLength = Transformations.baseNByteLength(base);
		for(int i = 0; i < data.size(); i++)
		{
			int numberToBeChanged = data.get(i);
			for(int j = 0; j < byteLength; j++)
			{
				tempList.add(numberToBeChanged%base);
				numberToBeChanged = (int) Math.floor(numberToBeChanged/base);
			}
			
			for(int k = tempList.size() - 1; k >= 0; k--)
			{
				convertedFileData.add(tempList.get(k));
			}
			tempList = new ArrayList<Integer>();
		}
		
		return convertedFileData;
	}
	
	private static ArrayList<Integer> transformDataFromBaseN(ArrayList<Integer> data, int base)
	{
		ArrayList<Integer> tempData = new ArrayList<Integer>();
		int byteLength = Transformations.baseNByteLength(base);
		Integer tempInt = 0;
		for(int i = 0; i < data.size(); i++)
		{
			tempInt = tempInt * base;
			tempInt = tempInt + data.get(i);
			if(i % byteLength == byteLength - 1)
			{
				tempData.add(tempInt);
				tempInt = 0;
			}
		}
		return tempData;
	}
	
	private static void setHeader(int encodeType, int base, boolean isEncrypted, String fileLocation, int dataLength)
	{
		//set encode type
		myHeader.encodeType.setEncodeType(encodeType);
						
		//set encode base
		myHeader.encodeBase.setEncodeBase(base);
					
		//set boolean isEncrypted
		myHeader.encodeEncryption.setEncodeEncryption(isEncrypted);
						
		//set file extension
		myHeader.fileExtension.setFileExtension(fileLocation);
						
		//set data length
		myHeader.encodeSize.setEncodeSize(dataLength);
	}
	
	private static int[] pixelRGB(int pixel) 
	{
//		int alpha = (pixel >> 24) & 0xff;
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		int[] id = {red, green, blue};
		return id;
	}
	
	private static int pixelRGB(int[] pixel) 
	{
		return ((pixel[0]&0x0ff)<<16)|((pixel[1]&0x0ff)<<8)|(pixel[2]&0x0ff);
	}
	
	private static BufferedImage loadImage(String imgLocation)
	{
		BufferedImage img = null;
		
		try 
		{
			img = ImageIO.read(new File(imgLocation));
		} 
		catch (IOException e) 
		{
			System.err.println("failed to read " + imgLocation);
			e.printStackTrace();
		}
		return img;
	}
	
	private static void saveImage(BufferedImage img, String imgLocation)
	{
        File file = new File(imgLocation);
        try 
        {
			ImageIO.write(img, "png", file);
		} 
        catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
