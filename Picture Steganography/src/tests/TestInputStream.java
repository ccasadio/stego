package tests;

import static org.junit.Assert.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fileStream.SteganographyBufferedInputStream;;

public class TestInputStream 
{
	private SteganographyBufferedInputStream myStream;
	private static final String mySampleFile = "./src/tests/sampleData";
	private int myBase = 4;
	
	@Before
	public void setUp() throws Exception 
	{
		myStream = new SteganographyBufferedInputStream(mySampleFile, myBase);
	}

	@After
	public void tearDown()
	{
		myStream = null;
		System.gc();
	}
	
	@Test
	public void testStreamRead1() throws IOException 
	{
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(mySampleFile));
		int read, byteLen = baseNByteLength(myBase);
		
		while((read = is.read()) != -1)
		{
			int[] data = base10ByteToBaseN(myBase, read);
			
			for(int i = 0; i < byteLen; i++)
			{
				assertTrue(data[i] == myStream.read());
			}
		}
	}

	@Test
	public void testStreamRead2() throws IOException 
	{
		BufferedInputStream is = new BufferedInputStream(new FileInputStream(mySampleFile));
		int[] data = new int[baseNByteLength(myBase)];
		int read, byteLen = baseNByteLength(myBase);
		
		while((read = is.read()) != -1)
		{
			for(int i = 0; i < byteLen; i++)
			{
				data[i] = myStream.read();
			}
			
			assertTrue(baseNByteToBase10(myBase, data) == read);
		}
	}
	
	
	
	
	
	
	
	
	
	private static int[] base10ByteToBaseN(int base, int data)
	{
		if(base < 2)
		{
			throw new IllegalArgumentException("Input base too small.");
		}
		
		int length = baseNByteLength(base);
		int[] transformedData = new int[length];
		
		int numberToBeChanged = data;
		for(int i = length - 1; i >= 0 ; i--)
		{
			transformedData[i] = numberToBeChanged%base;
			numberToBeChanged = (int) Math.floor(numberToBeChanged/base);
		}
		
		return transformedData;
	}
	
	private static int baseNByteToBase10(int base, int[] data)
	{
		if(base < 2)
		{
			throw new IllegalArgumentException("Input base too small.");
		}
		
		int temp = 0;
		
		for(int i = 0; i < data.length; i++)
		{
			temp = temp * base;
			temp = temp + data[i];
		}
		
		return temp;
	}
	
	private static int baseNByteLength(int base)
	{
		if(base < 2)
		{
			throw new IllegalArgumentException("base too small");
		}
	    else if(base == 2)
		{
			return 8;
		}
		else if(base == 3)
		{
			return 6;
		}
		else if(base <= 6)
		{
			return 4;
		}
		else if(base <= 15)
		{
			return 3;
		}
		else if(base == 16)
		{
			return 2;
		}
		else if(base > 16)
		{
			return 1;
		}
		
		return 0;
	}
}
